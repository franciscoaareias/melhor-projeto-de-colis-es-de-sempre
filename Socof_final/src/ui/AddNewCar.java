package ui;

import controllers.MainController;
import Languages.Language;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.event.*;
public class AddNewCar extends JFrame{
    
    private String[] list_of_file_names;
    private JLabel title, id_label, coordenates_label, x_label, y_label, z_label, car_label, image_label;
    private JButton create_button, cancel_button;
    private ClassLoader classLoader;
    private Language lang;
    private  boolean isCorrect = true;
    private JTextField x_field, y_field,z_field,id_field; 
    private MainController mainController;
    private int imageCar = -1;
    
    private static int high= 2;
    
    public AddNewCar()
    {
        lang = Language.getInstance();
        drawWindow();
        initLanguage();
    }
    public String[] getList_of_file_names() {
        return list_of_file_names;
    }

    public void setList_of_file_names(String[] list_of_file_names) {
        this.list_of_file_names = list_of_file_names;
    }

    public void setTitle(JLabel title) {
        this.title = title;
    }

    public JLabel getId_label() {
        return id_label;
    }

    public void setId_label(JLabel id_label) {
        this.id_label = id_label;
    }

    public JLabel getCoordenates_label() {
        return coordenates_label;
    }

    public void setCoordenates_label(JLabel coordenates_label) {
        this.coordenates_label = coordenates_label;
    }

    public JLabel getX_label() {
        return x_label;
    }

    public void setX_label(JLabel x_label) {
        this.x_label = x_label;
    }

    public JLabel getY_label() {
        return y_label;
    }

    public void setY_label(JLabel y_label) {
        this.y_label = y_label;
    }

    public JLabel getZ_label() {
        return z_label;
    }

    public void setZ_label(JLabel z_label) {
        this.z_label = z_label;
    }

    public JLabel getCar_label() {
        return car_label;
    }

    public void setCar_label(JLabel car_label) {
        this.car_label = car_label;
    }

    public JLabel getImage_label() {
        return image_label;
    }

    public void setImage_label(JLabel image_label) {
        this.image_label = image_label;
    }

    public JButton getCreate_button() {
        return create_button;
    }

    public void setCreate_button(JButton create_button) {
        this.create_button = create_button;
    }

    public JButton getCancel_button() {
        return cancel_button;
    }

    public void setCancel_button(JButton cancel_button) {
        this.cancel_button = cancel_button;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }
    private void drawWindow()
    {        
        
        mainController =  MainController.getInstance();
        // Sets the window size
        setSize(550, 470);
        
        // Sets the window to the center of the screen
        setLocationRelativeTo(null);
        
        // Closes the window when the 'X' button is pressed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        // Indicates that the window is not resizable
        setResizable(false);
        
        // Initiate the classloader object to load images
        classLoader = getClass().getClassLoader();
        
        // Creates the event listeners
        HandleFocusEvent handle_focus = new HandleFocusEvent();
        HandleButtonsEvents handle_buttons = new HandleButtonsEvents();
        
        // Container that has all the interface elements
        Container c = getContentPane();
        
        // Panel to use in the north of the window layout
        JPanel north_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        // Label used to define the window title
        title = new JLabel();
        title.setFont(new Font("Tahoma", 1, 24));
        title.setForeground(new Color(51, 0, 204));
        title.setPreferredSize(new Dimension(328, 37));
        
        // Panel to use in the center of the window layout
        JPanel center_panel = new JPanel(new BorderLayout());
        
        // Panel to use to display the car id
        JPanel id_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        // ID inner panel
        JPanel id_inner_panel = new JPanel(new BorderLayout());
        
        // Label used to ask the id of the new car
        id_label = new JLabel();
        id_label.setFont(new Font("Tahoma", 1, 16));
        id_label.setForeground(new Color(255, 0, 0));
        
        // Field corresponding to the car id
        id_field = new JTextField(4);
        id_field.setEditable(false);
        id_field.setBorder(BorderFactory.createEmptyBorder());
        id_field.setText(String.valueOf(mainController.getNumberOfCars()+1));
        
        // Label used to ask the coordinates of the car
        JPanel coordinate_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        coordenates_label = new JLabel();
        coordenates_label.setFont(new Font("Tahoma", 1, 16));
        coordenates_label.setForeground(new Color(255, 0, 0));
        
        // Panel to use to gather the rest of the center panel
        JPanel xyz_car_choice_panel = new JPanel(new GridBagLayout());
        
        // Panel that gathers all the coordinates information
        JPanel xyz_panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        // Label used to ask the x coordinate of the car
        x_label = new JLabel();
        x_label.setFont(new Font("Tahoma", 1, 12));
        
        // Field used to input the x coordinate of the car
        x_field = new JTextField(5);
        x_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        x_field.addFocusListener(handle_focus);
        
        // Label used to ask the y coordinate of the car
        y_label = new JLabel();
        y_label.setFont(new Font("Tahoma", 1, 12));
        
        // Field used to input the y coordinate of the car
        y_field = new JTextField(5);
        y_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        y_field.addFocusListener(handle_focus);
        
        // Label used to ask the y coordinate of the car
        z_label = new JLabel();
        z_label.setFont(new Font("Tahoma", 1, 12));
        
        // Field used to input the z coordinate of the car
        z_field = new JTextField(5);
        z_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        z_field.addFocusListener(handle_focus);
        
        // Label used to ask the dimensions of the car
        JPanel car_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        car_label = new JLabel();
        car_label.setFont(new Font("Tahoma", 1, 16));
        car_label.setForeground(new Color(255, 0, 0));
        
        // Splitted window
        // Panel containing the name of images
        String path = new File("src/images").getAbsolutePath();
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        
        // Array containing the name of files
        list_of_file_names = new String[listOfFiles.length];
        
        // Array containing the names
        String[] list_of_names = new String[listOfFiles.length];
        
        // Auxiliary array for spliting the file name in "."
        String[] auxiliary_array = new String[2];
        
        // Gets the files names
        for (int i = 0; i < listOfFiles.length; i++) {
            list_of_file_names[i] = listOfFiles[i].getName();
            auxiliary_array = list_of_file_names[i].split("\\.");
            list_of_names[i] = lang.getText(auxiliary_array[0]);
        }
        
        JPanel names_panel = new JPanel();
        
        JList jlist_of_names = new JList(list_of_names);
        jlist_of_names.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jlist_of_names.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        jlist_of_names.setSelectedIndex(0);
        jlist_of_names.addListSelectionListener(new HandleListEvent());
        
        JScrollPane listScroller = new JScrollPane(jlist_of_names);
        listScroller.setPreferredSize(new Dimension(150, 128));
        
        names_panel.add(listScroller);
        
        // Panel containing the the images
        JPanel image_panel = new JPanel();
        image_label = new JLabel();
        
        // Writes in the image panel
        ImageIcon car = new ImageIcon(classLoader.getResource("images/" + list_of_file_names[0]));
        imageCar = 0;
        image_label.setIcon(car);
        image_panel.add(image_label);

        
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, names_panel, image_panel);
        splitPane.setEnabled(false);
        
        // Panel to be used in the south of the window
        JPanel south_panel = new JPanel(new GridLayout(1, 3));
        
        JPanel left_south_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        JPanel right_south_panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        
        // Buttons to create and cancel
        create_button = new JButton();
        create_button.setPreferredSize(new Dimension(130, 25));
        create_button.addActionListener(handle_buttons);
        
        cancel_button = new JButton();
        cancel_button.setPreferredSize(new Dimension(85, 25));
        cancel_button.addActionListener(handle_buttons);
        
      
        // Sets the window layout
        // North panel
        north_panel.add(title);
        
        // Center panel
        id_panel.add(id_label);
        id_panel.add(id_field);
        id_inner_panel.add(id_panel);
        
        xyz_panel.add(x_label);
        xyz_panel.add(x_field);
        
        xyz_panel.add(y_label);
        xyz_panel.add(y_field);
        
        xyz_panel.add(z_label);
        xyz_panel.add(z_field);
        
        // GridBagConstraints to set the cells in the layout
        GridBagConstraints gbconstraints = new GridBagConstraints();
        gbconstraints.fill = GridBagConstraints.BOTH;
        gbconstraints.gridx = 0;
        gbconstraints.gridy = 0;
        
        GridBagConstraints gbconstraints_2 = new GridBagConstraints();
        gbconstraints_2.fill = GridBagConstraints.BOTH;
        gbconstraints_2.gridx = 0;
        gbconstraints_2.gridy = 1;
        gbconstraints_2.weighty = 0.1;
        
        GridBagConstraints gbconstraints_3 = new GridBagConstraints();
        gbconstraints_3.fill = GridBagConstraints.BOTH;
        gbconstraints_3.gridx = 0;
        gbconstraints_3.gridy = 2;
        gbconstraints_3.weightx = 0.5;
        gbconstraints_3.weighty = 0.1;
        
        GridBagConstraints gbconstraints_4 = new GridBagConstraints();
        gbconstraints_4.fill = GridBagConstraints.NORTH;
        gbconstraints_4.gridx = 0;
        gbconstraints_4.gridy = 3;
        gbconstraints_4.weighty = 0.1;
        
        GridBagConstraints gbconstraints_5 = new GridBagConstraints();
        gbconstraints_5.fill = GridBagConstraints.BOTH;
        gbconstraints_5.gridx = 0;
        gbconstraints_5.gridy = 4;
        gbconstraints_5.weighty = 0.1;
        
        coordinate_panel.add(coordenates_label);
        xyz_car_choice_panel.add(coordinate_panel, gbconstraints);
        xyz_car_choice_panel.add(xyz_panel, gbconstraints_2);
        car_panel.add(car_label);
        xyz_car_choice_panel.add(car_panel, gbconstraints_3);
        xyz_car_choice_panel.add(splitPane, gbconstraints_4);
        xyz_car_choice_panel.add(new JLabel(), gbconstraints_5);
        
        center_panel.add(id_inner_panel, BorderLayout.NORTH);
        center_panel.add(xyz_car_choice_panel, BorderLayout.CENTER);
        
        // South pane
        left_south_panel.add(create_button);
        right_south_panel.add(cancel_button);
        south_panel.add(left_south_panel);
        south_panel.add(right_south_panel);
        
        // Gather all elements in the container
        c.add(north_panel, BorderLayout.NORTH);
        c.add(center_panel, BorderLayout.CENTER);
        c.add(south_panel, BorderLayout.SOUTH);
        
        // Makes the window visible to the user
        
        setVisible(true);
    }
    
    private void initLanguage()
    {
        setTitle(lang.getText("title"));
        title.setText(lang.getText("menu_add_car_add"));
        id_label.setText(lang.getText("menu_add_car_id"));
        coordenates_label.setText(lang.getText("menu_add_car_coordinates"));
        x_label.setText(lang.getText("menu_add_car_x"));
        y_label.setText(lang.getText("menu_add_car_y"));
        z_label.setText(lang.getText("menu_add_car_z"));
        car_label.setText(lang.getText("menu_add_car_car"));
        create_button.setText(lang.getText("menu_add_car_create"));
        cancel_button.setText(lang.getText("menu_add_car_cancel"));
    }
     //Método para verificar se a String é double
    public boolean verificarDouble(String x){
        double numero;
        try{ 
            numero = Double.parseDouble(x);
            return true;
        }catch(NumberFormatException e){  
            return false;  
        }
    }
    
    class HandleListEvent implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent lse) {
            if (!lse.getValueIsAdjusting()) {

                // Gets the selected value
                JList source = (JList) lse.getSource();
                int selected = source.getSelectedIndex();
                
                // Refreshes the image
                ImageIcon car = new ImageIcon(classLoader.getResource("images/" + list_of_file_names[selected]));
                imageCar = selected;
                car.getImage().flush();
                image_label.setIcon(car);
                
            }
        }
    }
    
    class HandleFocusEvent implements FocusListener
    {
        @Override
        public void focusGained(FocusEvent fe) {
            ((JTextField)(fe.getComponent())).setBorder(BorderFactory.createLineBorder(Color.blue));
        }

        @Override
        public void focusLost(FocusEvent fe) {
            ((JTextField)(fe.getComponent())).setBorder(BorderFactory.createLineBorder(Color.darkGray));
        }
        
    }
    
    class HandleButtonsEvents implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if(ae.getSource() == create_button)
            {
                    isCorrect = true;
                    
                   if(verificarDouble(x_field.getText())==false)
                   {
                       x_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                       isCorrect = false;
                   } 
                  
                   
                   if(verificarDouble(y_field.getText()) == false)
                   {
                       y_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                       isCorrect = false;
                   }
                   
                   
                   if(verificarDouble(z_field.getText()) == false)
                   {
                       z_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                       isCorrect = false;
                   }
                  
                   
                  if(isCorrect == true && imageCar != -1)
                  {
                     Icon image_icon = image_label.getIcon();
                   
                     mainController.create(mainController.getNumberOfCars()+1, x_field.getText(), y_field.getText(),z_field.getText(),image_icon.getIconWidth(), image_icon.getIconHeight(),high,imageCar);
                     System.out.println("Criou carro");
                     
                      x_field.setText("");
                      y_field.setText("");
                      z_field.setText("");
                      
                      id_field.setText(String.valueOf(mainController.getNumberOfCars()+1));
                  
                  }
                
                
            }
            else if(ae.getSource() == cancel_button)
                dispose();
        }
    }
    
    
}
