package ui;

import Languages.Language;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.*;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import javax.swing.*;
import javax.swing.border.Border;

public class MainWindow extends JFrame{

    private JLabel title;
    private Language lang;
    private JButton add_button, edit_button,startDetectingCollision;
    private JMenuItem add_cars, edit_cars, lang_pt, lang_en;
    private Container c;
    private HandleActionEvents handle_events;
    private MainWindow main;
    private Clip clip;
    private JCheckBoxMenuItem check_music_1, check_music_2, check_music_3;
    
    public MainWindow()
    {
        lang = Language.getInstance();
        drawMenus();
        drawWindow();
        title.setText(lang.getText("title"));
        setTitle(lang.getText("title"));
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    public JButton getAdd_button() {
        return add_button;
    }

    public void setAdd_button(JButton add_button) {
        this.add_button = add_button;
    }

    public JButton getEdit_button() {
        return edit_button;
    }

    public void setEdit_button(JButton edit_button) {
        this.edit_button = edit_button;
    }

    public JMenuItem getAdd_cars() {
        return add_cars;
    }

    public void setAdd_cars(JMenuItem add_cars) {
        this.add_cars = add_cars;
    }

    public JMenuItem getEdit_cars() {
        return edit_cars;
    }

    public void setEdit_cars(JMenuItem edit_cars) {
        this.edit_cars = edit_cars;
    }

    public JMenuItem getLang_pt() {
        return lang_pt;
    }

    public void setLang_pt(JMenuItem lang_pt) {
        this.lang_pt = lang_pt;
    }

    public JMenuItem getLang_en() {
        return lang_en;
    }

    public void setLang_en(JMenuItem lang_en) {
        this.lang_en = lang_en;
    }

    public Container getC() {
        return c;
    }

    public void setC(Container c) {
        this.c = c;
    }

    public HandleActionEvents getHandle_events() {
        return handle_events;
    }

    public void setHandle_events(HandleActionEvents handle_events) {
        this.handle_events = handle_events;
    }

    public MainWindow getMain() {
        return main;
    }

    public void setMain(MainWindow main) {
        this.main = main;
    }

    public Clip getClip() {
        return clip;
    }

    public void setClip(Clip clip) {
        this.clip = clip;
    }

    public JCheckBoxMenuItem getCheck_music_1() {
        return check_music_1;
    }

    public void setCheck_music_1(JCheckBoxMenuItem check_music_1) {
        this.check_music_1 = check_music_1;
    }

    public JCheckBoxMenuItem getCheck_music_2() {
        return check_music_2;
    }

    public void setCheck_music_2(JCheckBoxMenuItem check_music_2) {
        this.check_music_2 = check_music_2;
    }

    public JCheckBoxMenuItem getCheck_music_3() {
        return check_music_3;
    }

    public void setCheck_music_3(JCheckBoxMenuItem check_music_3) {
        this.check_music_3 = check_music_3;
    }
    
    
    
    private void drawWindow()
    {        
         // Sets the window size
        setSize(550, 470);
        
        // Sets the window to the center of the screen
        setLocationRelativeTo(null);
        
        // Closes the window when the 'X' button is pressed
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Indicates that the window is not resizable
        setResizable(false);
        
        // Initiate the classloader object to load images
        ClassLoader classLoader = getClass().getClassLoader();
        
        HandleWindowEvents handle_window = new HandleWindowEvents();
        
        this.addWindowListener(handle_window);
        
        // Container that has all the interface elements
        c = getContentPane();
        
        // Panel to use in the north of the window layout
        JPanel north_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        // Label used to define the window title
        title = new JLabel();
        title.setFont(new Font("Tahoma", 1, 24));
        title.setForeground(new Color(51, 0, 204));
        title.setPreferredSize(new Dimension(328, 37));
        
        // Panel to use in the center of the window layout
        JPanel center_panel = new JPanel(new BorderLayout());
        
        // JLayredPane to draw the race track and the cars
        JLayeredPane layered_pane = new JLayeredPane();
        layered_pane.setLayout(null);
        
        // Add the race track image
        JLabel background = new JLabel();
        URL imageURL = classLoader.getResource("tracks/crossing.jpg");
        background.setIcon(new ImageIcon(imageURL));
        background.setBounds(10, 0, 549, 362);
        layered_pane.add(background, -1);
        
        // Creates 2 cars
        JLabel car_1 = new JLabel();
        URL car_1_url = classLoader.getResource("images/bus.png");
        car_1.setIcon(new ImageIcon(car_1_url));
        car_1.setBounds(60, 10, 150, 70);
        layered_pane.add(car_1, 0);

        JLabel car_2 = new JLabel();
        URL car_2_url = classLoader.getResource("images/car.png");
        car_2.setIcon(new ImageIcon(car_2_url));
        car_2.setBounds(80, 100, 150, 100);
        layered_pane.add(car_2, 0);
        
        JPanel south_panel = new JPanel(new BorderLayout());
         startDetectingCollision = new JButton("Start");
        startDetectingCollision.setVisible(true);
        startDetectingCollision.addActionListener(handle_events);
        south_panel.add(startDetectingCollision);
        
        // Panel to gather the buttons on the right corner
        JPanel images_panel = new JPanel(new GridLayout(5, 1, 0, 10));
        
        // Add button
        JPanel add_panel = new JPanel();
        add_button = new JButton();
        ImageIcon add_img = new ImageIcon(classLoader.getResource("icons/add_car_icon.png"));
        
        BufferedImage resizedImage_car = new BufferedImage(60,50, 1);
        Graphics2D g_car = resizedImage_car.createGraphics();
        g_car.drawImage(add_img.getImage(), 0, 0, 60,50, null);
        g_car.dispose();
        ImageIcon tmp_car = new ImageIcon(resizedImage_car);
        
        add_button.setIcon(tmp_car);
        add_button.setPreferredSize(new Dimension(60,50));
        add_button.addActionListener(handle_events);
        
        add_panel.add(add_button);
        
        // Edit button
        JPanel edit_panel = new JPanel();
        edit_button = new JButton();
        ImageIcon edit_img = new ImageIcon(classLoader.getResource("icons/edit_car_icon.gif"));
        
        resizedImage_car = new BufferedImage(60,50, 1);
        g_car = resizedImage_car.createGraphics();
        g_car.drawImage(edit_img.getImage(), 0, 0, 60, 50, null);
        g_car.dispose();
        
        tmp_car = new ImageIcon(resizedImage_car);
        
        edit_button.setIcon(tmp_car);
        edit_button.setPreferredSize(new Dimension(60, 50));
        edit_button.addActionListener(handle_events);
        
        edit_panel.add(edit_button);
        
        // Adds the elements to the container
        north_panel.add(title);
        
        images_panel.add(add_panel);
        images_panel.add(edit_panel);
        center_panel.add(images_panel, BorderLayout.EAST);
        
        c.add(north_panel, BorderLayout.NORTH);
        c.add(center_panel, BorderLayout.EAST);
        c.add(layered_pane, BorderLayout.CENTER);
        c.add(south_panel, BorderLayout.SOUTH);
        
        setVisible(true);
    }
    
      private void drawMenus()
    {
        // Creates a new JMenuBar
        JMenuBar jmenubar = new JMenuBar();
        
        // Creates the event listeners
        handle_events = new HandleActionEvents();
        
        // Creates the car menu
        JMenu menu_cars = new JMenu();
        menu_cars.setText(lang.getText("menu_opt_cars"));
        menu_cars.setMnemonic(KeyEvent.VK_C);
        jmenubar.add(menu_cars);
        
        // Adds the submenus to the menu car
        // Option to add new cars
        add_cars = new JMenuItem();
        add_cars.setText(lang.getText("menu_cars_opt_add"));
        add_cars.addActionListener(handle_events);
        menu_cars.add(add_cars);

        // Option to edit cars
        edit_cars = new JMenuItem();
        edit_cars.setText(lang.getText("menu_cars_opt_edit"));
        edit_cars.addActionListener(handle_events);
        menu_cars.add(edit_cars);
        
        // Options menu
        JMenu menu_opt = new JMenu();
        menu_opt.setText(lang.getText("menu_opt_options"));
        menu_opt.setMnemonic(KeyEvent.VK_O);
        jmenubar.add(menu_opt);
        
        // Adds the submenus to the menu options
        // Option to add sounds
        JMenuItem sound = new JMenuItem();
        sound.setText(lang.getText("menu_options_opt_sounds"));
        menu_opt.add(sound);
        
        // Option to add music
        JMenu music = new JMenu();
        music.setText(lang.getText("menu_options_opt_music"));
        menu_opt.add(music);
        
        // Option to add music        
        check_music_1 = new JCheckBoxMenuItem(lang.getText("track_1"));
        check_music_1.addActionListener(handle_events);
        check_music_1.setEnabled(true);
        music.add(check_music_1);
        
        
        // Option to add music
        check_music_2 = new JCheckBoxMenuItem(lang.getText("track_2"));
        check_music_2.addActionListener(handle_events);
        check_music_2.setEnabled(true);
        music.add(check_music_2);
        
        // Option to add music
        check_music_3 = new JCheckBoxMenuItem(lang.getText("track_3"));
        check_music_3.addActionListener(handle_events);
        check_music_3.setEnabled(true);
        music.add(check_music_3);
        
        // Option to change language
        JMenu language = new JMenu();
        language.setText(lang.getText("menu_options_opt_lang"));
        menu_opt.add(language);
        
        // Portuguese
        lang_pt = new JMenuItem();
        lang_pt.setText(lang.getText("lang_pt"));
        lang_pt.addActionListener(handle_events);
        language.add(lang_pt);
        
        //English
        lang_en = new JMenuItem();
        lang_en.setText(lang.getText("lang_en"));
        lang_en.addActionListener(handle_events);
        language.add(lang_en);
        
        setJMenuBar(jmenubar);
        
        
    }

    class HandleActionEvents implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if(ae.getSource() == add_button || ae.getSource() == add_cars)
            {
                // Opens the add car window
                AddNewCar add = new AddNewCar();
            }
            else if(ae.getSource() == edit_button || ae.getSource() == edit_cars)
            {
                // Opens the edit car window
                EditCar edit = new EditCar();
            }
            else if(ae.getSource() == startDetectingCollision )
            {
                MapWindow mapWindow = new MapWindow();
            }
            else if(ae.getSource() == check_music_1)
            {
                if (check_music_2.isSelected()) {
                    check_music_2.setSelected(false);
                    clip.stop();
                    clip.close();
                } else if (check_music_3.isSelected()) {
                    check_music_3.setSelected(false);
                    clip.stop();
                    clip.close();
                }
                if(clip == null || !clip.isRunning()) 
                {
                    try {
                        clip = AudioSystem.getClip();
                        File f = new File(new File("src/audio/hit_the_road.wav").getAbsolutePath());
                        AudioInputStream ais = AudioSystem.getAudioInputStream(f);
                        clip.open(ais);
                        clip.loop(Clip.LOOP_CONTINUOUSLY);
                    } catch (Exception ex) {
                        dispose();
                    }
                }
                else if(clip.isRunning())
                {
                    clip.stop();
                    clip.close();
                }
            }
            else if(ae.getSource() == check_music_2)
            {
                if (check_music_1.isSelected()) {
                    check_music_1.setSelected(false);
                    clip.stop();
                    clip.close();
                } else if (check_music_3.isSelected()) {
                    check_music_3.setSelected(false);
                    clip.stop();
                    clip.close();
                }
                if(clip == null || !clip.isRunning()) 
                {
                    try {
                        clip = AudioSystem.getClip();
                        File f = new File(new File("src/audio/open_road.wav").getAbsolutePath());
                        AudioInputStream ais = AudioSystem.getAudioInputStream(f);
                        clip.open(ais);
                        clip.loop(Clip.LOOP_CONTINUOUSLY);
                    } catch (Exception ex) {
                        dispose();
                    }
                }
                else if(clip.isRunning())
                {
                    clip.stop();
                    clip.close();
                }
            }
            else if(ae.getSource() == check_music_3)
            {
                if (check_music_2.isSelected()) {
                    check_music_2.setSelected(false);
                    clip.stop();
                    clip.close();
                } else if (check_music_1.isSelected()) {
                    check_music_1.setSelected(false);
                    clip.stop();
                    clip.close();
                }
                if(clip == null || !clip.isRunning()) 
                {
                    try {
                        clip = AudioSystem.getClip();
                        File f = new File(new File("src/audio/highway_to_hell.wav").getAbsolutePath());
                        AudioInputStream ais = AudioSystem.getAudioInputStream(f);
                        clip.open(ais);
                        clip.loop(Clip.LOOP_CONTINUOUSLY);
                    } catch (Exception ex) {
                        dispose();
                    }
                }
                else if(clip.isRunning())
                {
                    clip.stop();
                    clip.close();
                }
            }
            else if(ae.getSource() == lang_pt)
            {
                // Changes the language to portuguese
                lang.setLang("pt");
                dispose();
                main = new MainWindow();
            }
            else if(ae.getSource() == lang_en)
            {
                // Changes the language to english
                lang.setLang("en");
                dispose();
                main = new MainWindow();
            }
        }
    }
    
    public class HandleWindowEvents extends WindowAdapter
    {
        public void windowClosed(WindowEvent we) {
            if(clip != null)
                clip.stop();
        }
    }
}
