package ui;

import controllers.StartDetectingCollisionController;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.event.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;
import model.Coordinate;

public class MapWindow extends JFrame {

    private final Timer executeTask;
    private final static int timeToRefresh = 1500;
    private int var;
    private StartDetectingCollisionController controller;
    private StopButtonListener stopButton;
    private static final int xLimit=5;
    private static final int yLimit=5;

    public MapWindow() {
        this.var = 1; //temporary var

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        stopButton = new StopButtonListener();

        controller = StartDetectingCollisionController.getInstance();

        HandleActionEvents handle_action = new HandleActionEvents();
        executeTask = new Timer(timeToRefresh, handle_action);

        HandleWindowEvents handle = new HandleWindowEvents();
        this.addWindowListener(handle);

        executeTask.start();

        controller.startDetectingCollisionThreads();

        setSize(1280, 1024);
        setBackground(Color.white);
        setResizable(false);
        setVisible(true);

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        for (Coordinate c : controller.getNextCoordinates()) {
            g.setColor(Color.red);
          if(c.getX() + xLimit >= this.getWidth())
          {
              //this will collide with the x axis
              
              //will veriify if the car will collide with the y axis comparated with the y size of the window
              if(c.getY() + yLimit >= this.getHeight())
              {
                 
              }
              else
              {
                  
              }
              
          }else if(c.getY() + yLimit >=  this.getWidth())
          {
              
              
          }
          else              
          {
            g.fillRect(((int) (c.getX())), ((int) (c.getY())), 20, 30);
            g.drawRect(((int) (c.getX())), ((int) (c.getY())), 20, 30);
           
          } 
          
           
            
        }
    }

    public class HandleActionEvents implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {

            repaint();
        }
    }

    public class HandleWindowEvents extends WindowAdapter {

        @Override
        public void windowClosed(WindowEvent we) {
            executeTask.stop();
            controller.stopDetectingCollisionThreads();

        }
    }

    /* Stops detecting the collision */
    public class StopButtonListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            JButton button = (JButton)  e.getSource();
            executeTask.stop();
            dispose();
        }

    }

}
