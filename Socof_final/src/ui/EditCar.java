package ui;

import controllers.MainController;
import Languages.Language;
import java.awt.*;
import java.awt.event.*;
import static java.awt.image.ImageObserver.ALLBITS;
import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.Car;

public class EditCar extends JFrame {

    private String[] list_of_file_names;
    private JLabel title, id_label, coordenates_label, x_label, y_label, z_label, car_label, image_label;
    private JButton delete_button, edit_button, cancel_button;
    private ClassLoader classLoader;
    private Language lang;
    private MainController mainController;
    private JComboBox cars_list;
    private JTextField x_field, y_field,z_field;
    private Car carToEdit = null;
   private ImageIcon imageCarEdited = null;
   private int indexImageIcon = -1;

    public EditCar() {
        lang = Language.getInstance();
        drawWindow();
        initLanguage();
    }

    private void drawWindow() {
        mainController = MainController.getInstance();
        // Sets the window size
        setSize(550, 470);

        // Sets the window to the center of the screen
        setLocationRelativeTo(null);

        // Closes the window when the 'X' button is pressed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Indicates that the window is not resizable
        setResizable(false);

        // Initiate the classloader object to load images
        classLoader = getClass().getClassLoader();

        // Creates the event listeners
        HandleFocusEvent handle_focus = new HandleFocusEvent();
        HandleButtonsEvents handle_buttons = new HandleButtonsEvents();

        // Container that has all the interface elements
        Container c = getContentPane();

        // Panel to use in the north of the window layout
        JPanel north_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        // Label used to define the window title
        title = new JLabel();
        title.setFont(new Font("Tahoma", 1, 24));
        title.setForeground(new Color(51, 0, 204));
        title.setPreferredSize(new Dimension(328, 37));

        // Panel to use in the center of the window layout
        JPanel center_panel = new JPanel(new BorderLayout());

        // Panel to use to display the car id
        JPanel id_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        // ID inner panel
        JPanel id_inner_panel = new JPanel(new BorderLayout());

        // Label used to ask the id of the new car
        id_label = new JLabel();
        id_label.setFont(new Font("Tahoma", 1, 16));
        id_label.setForeground(new Color(255, 0, 0));

        // Combobox with the existing cars
        cars_list = new JComboBox(mainController.getIdOfCars().toArray());
        cars_list.setPreferredSize(new Dimension(130, 25));
        cars_list.setSelectedItem(null);
        cars_list.addItemListener(new HandleCombobox());

        delete_button = new JButton();
        delete_button.setPreferredSize(new Dimension(80, 25));
        delete_button.addActionListener(handle_buttons);

        // Label used to ask the coordinates of the car
        JPanel coordinate_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        coordenates_label = new JLabel();
        coordenates_label.setFont(new Font("Tahoma", 1, 16));
        coordenates_label.setForeground(new Color(255, 0, 0));

        // Panel to use to gather the rest of the center panel
        JPanel xyz_car_choice_panel = new JPanel(new GridBagLayout());

        // Panel that gathers all the coordinates information
        JPanel xyz_panel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        // Label used to ask the x coordinate of the car
        x_label = new JLabel();
        x_label.setFont(new Font("Tahoma", 1, 12));

        // Field used to input the x coordinate of the car
        x_field = new JTextField(5);
        x_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        x_field.addFocusListener(handle_focus);

        // Label used to ask the y coordinate of the car
        y_label = new JLabel();
        y_label.setFont(new Font("Tahoma", 1, 12));

        // Field used to input the y coordinate of the car
        y_field = new JTextField(5);
        y_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        y_field.addFocusListener(handle_focus);

        // Label used to ask the y coordinate of the car
        z_label = new JLabel();
        z_label.setFont(new Font("Tahoma", 1, 12));

        // Field used to input the z coordinate of the car
        z_field = new JTextField(5);
        z_field.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        z_field.addFocusListener(handle_focus);

        // Label used to ask the dimensions of the car
        JPanel car_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        car_label = new JLabel();
        car_label.setFont(new Font("Tahoma", 1, 16));
        car_label.setForeground(new Color(255, 0, 0));

        // Splitted window
        // Panel containing the name of images
        String path = new File("src/images").getAbsolutePath();
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        // Array containing the name of files
        list_of_file_names = new String[listOfFiles.length];

        // Array containing the names
        String[] list_of_names = new String[listOfFiles.length];

        // Auxiliary array for spliting the file name in "."
        String[] auxiliary_array = new String[2];

        // Gets the files names
        for (int i = 0; i < listOfFiles.length; i++) {
            list_of_file_names[i] = listOfFiles[i].getName();
            auxiliary_array = list_of_file_names[i].split("\\.");
            list_of_names[i] = lang.getText(auxiliary_array[0]);
        }

        JPanel names_panel = new JPanel();

        JList jlist_of_names = new JList(list_of_names);
        jlist_of_names.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jlist_of_names.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        jlist_of_names.setSelectedIndex(0);
        jlist_of_names.addListSelectionListener(new HandleListEvent());

        JScrollPane listScroller = new JScrollPane(jlist_of_names);
        listScroller.setPreferredSize(new Dimension(150, 128));

        names_panel.add(listScroller);

        // Panel containing the the images
        JPanel image_panel = new JPanel();
        image_label = new JLabel();

        // Writes in the image panel
        ImageIcon car = new ImageIcon(classLoader.getResource("images/" + list_of_file_names[0]));
       // imageCarEdited = car;
        image_label.setIcon(car);
        image_panel.add(image_label);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, names_panel, image_panel);
        splitPane.setEnabled(false);

        // Panel to be used in the south of the window
        JPanel south_panel = new JPanel(new GridLayout(1, 3));

        JPanel left_south_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        JPanel right_south_panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        // Buttons to create and cancel
        edit_button = new JButton();
        edit_button.setPreferredSize(new Dimension(85, 25));
        edit_button.addActionListener(handle_buttons);

        cancel_button = new JButton();
        cancel_button.setPreferredSize(new Dimension(85, 25));
        cancel_button.addActionListener(handle_buttons);

        // Sets the window layout
        // North panel
        north_panel.add(title);

        // Center panel
        id_panel.add(id_label);
        id_panel.add(cars_list);
        id_panel.add(delete_button);
        id_inner_panel.add(id_panel);

        xyz_panel.add(x_label);
        xyz_panel.add(x_field);

        xyz_panel.add(y_label);
        xyz_panel.add(y_field);

        xyz_panel.add(z_label);
        xyz_panel.add(z_field);

        // GridBagConstraints to set the cells in the layout
        GridBagConstraints gbconstraints = new GridBagConstraints();
        gbconstraints.fill = GridBagConstraints.BOTH;
        gbconstraints.gridx = 0;
        gbconstraints.gridy = 0;

        GridBagConstraints gbconstraints_2 = new GridBagConstraints();
        gbconstraints_2.fill = GridBagConstraints.BOTH;
        gbconstraints_2.gridx = 0;
        gbconstraints_2.gridy = 1;
        gbconstraints_2.weighty = 0.1;

        GridBagConstraints gbconstraints_3 = new GridBagConstraints();
        gbconstraints_3.fill = GridBagConstraints.BOTH;
        gbconstraints_3.gridx = 0;
        gbconstraints_3.gridy = 2;
        gbconstraints_3.weightx = 0.5;
        gbconstraints_3.weighty = 0.1;

        GridBagConstraints gbconstraints_4 = new GridBagConstraints();
        gbconstraints_4.fill = GridBagConstraints.NORTH;
        gbconstraints_4.gridx = 0;
        gbconstraints_4.gridy = 3;
        gbconstraints_4.weighty = 0.1;

        GridBagConstraints gbconstraints_5 = new GridBagConstraints();
        gbconstraints_5.fill = GridBagConstraints.BOTH;
        gbconstraints_5.gridx = 0;
        gbconstraints_5.gridy = 4;
        gbconstraints_5.weighty = 0.1;

        coordinate_panel.add(coordenates_label);
        xyz_car_choice_panel.add(coordinate_panel, gbconstraints);
        xyz_car_choice_panel.add(xyz_panel, gbconstraints_2);
        car_panel.add(car_label);
        xyz_car_choice_panel.add(car_panel, gbconstraints_3);
        xyz_car_choice_panel.add(splitPane, gbconstraints_4);
        xyz_car_choice_panel.add(new JLabel(), gbconstraints_5);

        center_panel.add(id_inner_panel, BorderLayout.NORTH);
        center_panel.add(xyz_car_choice_panel, BorderLayout.CENTER);

        // South pane
        left_south_panel.add(edit_button);
        right_south_panel.add(cancel_button);
        south_panel.add(left_south_panel);
        south_panel.add(right_south_panel);

        // Gather all elements in the container
        c.add(north_panel, BorderLayout.NORTH);
        c.add(center_panel, BorderLayout.CENTER);
        c.add(south_panel, BorderLayout.SOUTH);

        // Makes the window visible to the user
        setVisible(true);
    }

    public String[] getList_of_file_names() {
        return list_of_file_names;
    }

    public void setList_of_file_names(String[] list_of_file_names) {
        this.list_of_file_names = list_of_file_names;
    }

    public JLabel getId_label() {
        return id_label;
    }

    public void setId_label(JLabel id_label) {
        this.id_label = id_label;
    }

    public JLabel getCoordenates_label() {
        return coordenates_label;
    }

    public void setCoordenates_label(JLabel coordenates_label) {
        this.coordenates_label = coordenates_label;
    }

    public JLabel getX_label() {
        return x_label;
    }

    public void setX_label(JLabel x_label) {
        this.x_label = x_label;
    }

    public JLabel getY_label() {
        return y_label;
    }

    public void setY_label(JLabel y_label) {
        this.y_label = y_label;
    }

    public JLabel getZ_label() {
        return z_label;
    }

    public void setZ_label(JLabel z_label) {
        this.z_label = z_label;
    }

    public JLabel getCar_label() {
        return car_label;
    }

    public void setCar_label(JLabel car_label) {
        this.car_label = car_label;
    }

    public JLabel getImage_label() {
        return image_label;
    }

    public void setImage_label(JLabel image_label) {
        this.image_label = image_label;
    }

    public JButton getDelete_button() {
        return delete_button;
    }

    public void setDelete_button(JButton delete_button) {
        this.delete_button = delete_button;
    }

    public JButton getEdit_button() {
        return edit_button;
    }

    public void setEdit_button(JButton edit_button) {
        this.edit_button = edit_button;
    }

    public JButton getCancel_button() {
        return cancel_button;
    }

    public void setCancel_button(JButton cancel_button) {
        this.cancel_button = cancel_button;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }
    

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }
    
   
    
    private void initLanguage() {
        setTitle(lang.getText("title"));
        title.setText(lang.getText("menu_edit_car_edit"));
        id_label.setText(lang.getText("menu_edit_car_choose"));
        delete_button.setText(lang.getText("menu_edit_car_delete"));
        coordenates_label.setText(lang.getText("menu_edit_car_coordinates"));
        x_label.setText(lang.getText("menu_edit_car_x"));
        y_label.setText(lang.getText("menu_edit_car_y"));
        z_label.setText(lang.getText("menu_edit_car_z"));
        car_label.setText(lang.getText("menu_edit_car_car"));
        edit_button.setText(lang.getText("menu_edit_car_edit_bt"));
        cancel_button.setText(lang.getText("menu_edit_car_cancel"));
    }

    class HandleListEvent implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent lse) {
            if (!lse.getValueIsAdjusting()) {

                // Gets the selected value
                JList source = (JList) lse.getSource();
                int selected = source.getSelectedIndex();

                // Refreshes the image
                ImageIcon car = new ImageIcon(classLoader.getResource("images/" + list_of_file_names[selected]));
                imageCarEdited=car;
                indexImageIcon = selected;
                car.getImage().flush();
                image_label.setIcon(car);
            }
        }
    }

    class HandleFocusEvent implements FocusListener {

        @Override
        public void focusGained(FocusEvent fe) {
            ((JTextField) (fe.getComponent())).setBorder(BorderFactory.createLineBorder(Color.blue));
        }

        @Override
        public void focusLost(FocusEvent fe) {
            ((JTextField) (fe.getComponent())).setBorder(BorderFactory.createLineBorder(Color.darkGray));
        }

    }

    class HandleButtonsEvents implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == edit_button) {
                boolean status = true;
                
                if(!mainController.verifyDouble(x_field.getText()))
                {
                    status = false;
                     x_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                }
                if(!mainController.verifyDouble(y_field.getText()))
                {
                    status = false;
                     y_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                }
                if(!mainController.verifyDouble(z_field.getText()))
                {
                    status = false;
                    z_field.setBorder(BorderFactory.createLineBorder(Color.RED));
                }
                
                if(status)
                {
                    if(imageCarEdited !=null && indexImageIcon != -1)
                    {
                        mainController.edit(carToEdit, carToEdit.getIdCar(),x_field.getText() , y_field.getText(), z_field.getText(), imageCarEdited.getIconWidth(), carToEdit.getCarLength(), imageCarEdited.getIconHeight(), indexImageIcon);
                        
                    }
                    else
                    {
                        mainController.edit(carToEdit, carToEdit.getIdCar(),x_field.getText() , y_field.getText(), z_field.getText(), carToEdit.getCarWidth(), carToEdit.getCarLength(), carToEdit.getCarHeight(),carToEdit.getCarImage());
                    }
                    
                }
                        
            } else if (ae.getSource() == cancel_button) {
                dispose();
            } else if (ae.getSource() == delete_button) {
                   int index = cars_list.getSelectedIndex();
                   if(index != -1)
                   {
                       if(mainController.containsIdCar(index+1))
                       {
                           mainController.deleteCar(index+1);
                           cars_list.repaint();
                            x_field.setText("");
                            y_field.setText("");
                            z_field.setText("");
                       }
                   }
            } 
        }
    }

    class HandleCombobox implements ItemListener { 

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getSource() == cars_list) {
                
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    int idCar = (int) e.getItem();
                    if(mainController.containsIdCar(idCar))
                    {
                        carToEdit = mainController.getCar(idCar);
                        x_field.setText(Double.toString(carToEdit.getCurrentCoordiante().getX()));
                        y_field.setText(Double.toString(carToEdit.getCurrentCoordiante().getY()));
                        z_field.setText(Double.toString(carToEdit.getCurrentCoordiante().getZ()));
                        
                             // Refreshes the image
                        ImageIcon car = new ImageIcon(classLoader.getResource("images/" + list_of_file_names[carToEdit.getCarImage()]));
                        imageCarEdited=car;
                        indexImageIcon = carToEdit.getCarImage();
                        car.getImage().flush();
                        image_label.setIcon(car);
                    }
                }
            }
        }
    }
    
    

}
