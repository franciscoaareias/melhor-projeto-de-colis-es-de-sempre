package model;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Car extends Thread {

    //Variables
    private int idCar;
    //car priority
    private long priority;
    //current coordinate
    private Coordinate currentCoordiante;
    //Coordenada no eixo dos x
    private int carWidth;
    //Coordenada no eixo dos y
    private int carLength;
    //Coordenada no eixo dos z
    private int carHeight;
    //image that appears in the map
    private int carImage;

    //validate if the thread has to wait
    private boolean isWaiting;
    //increment to the position of the car
    private long increment = 10;

    private final Object lock = new Object();

    private Coordinate next_coordinate;

    private double distanceTravel;

    private double speed;

    private Map map;

    private ArrayList<Collision> collisions;

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getCarImage() {
        return carImage;
    }

    public void setCarImage(int carImage) {
        this.carImage = carImage;
    }

    //Constructors
    public Car() {
        this.isWaiting = false;
    }

    public Car(int idCar, int carWidth, int carLength) {
        this.idCar = idCar;
        this.carWidth = carWidth;
        this.carLength = carLength;
        this.next_coordinate = null;
        this.distanceTravel = 0.0;
        this.speed = 0.0;
        this.map = Map.getInstance();
        this.isWaiting = false;
        this.collisions = new ArrayList();
    }

    public Car(int idCar, int carWidth, int carLength, int carHeight) {
        this.idCar = idCar;
        this.carWidth = carWidth;
        this.carLength = carLength;
        this.carHeight = carHeight;
        this.collisions = new ArrayList();
        this.next_coordinate = null;
        this.distanceTravel = 0.0;
        this.speed = 0.0;
        this.map = Map.getInstance();
        this.isWaiting = false;
    }

    public Car(int idCar, Coordinate coordiante, int carWidth, int carLength, int carHeight, long priority) {
        this.idCar = idCar;
        this.currentCoordiante = coordiante;
        this.carWidth = carWidth;
        this.carLength = carLength;
        this.carHeight = carHeight;
        setPriority(priority);
        this.next_coordinate = null;
        this.distanceTravel = 0.0;
        this.speed = 0.0;
        this.map = Map.getInstance();
        this.isWaiting = false;
        this.collisions = new ArrayList();
    }

    public ArrayList<Collision> getCollisions() {
        return collisions;
    }

    public void setCollisions(ArrayList<Collision> collisions) {
        this.collisions = collisions;
    }

    //Gets e Sets
    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public Coordinate getNext_coordinate() {
        return next_coordinate;
    }

    public void setNext_coordinate(Coordinate next_coordinate) {
        this.next_coordinate = next_coordinate;
    }

    public long getCarPriority() {
        return priority;
    }

    private void setPriority(long newPriority) {
        this.priority = newPriority;
    }

    public boolean isIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(boolean isWaiting) {
        this.isWaiting = isWaiting;
    }

    public Coordinate getCurrentCoordiante() {
        return currentCoordiante;
    }

    public void setCurrentCoordiante(Coordinate coordiante) {
        this.currentCoordiante = coordiante;
    }

    public int getCarWidth() {
        return carWidth;
    }

    public void setCarWidth(int carWidth) {
        this.carWidth = carWidth;
    }

    public int getCarLength() {
        return carLength;
    }

    public void setCarLength(int carLength) {
        this.carLength = carLength;
    }

    public int getCarHeight() {
        return carHeight;
    }

    public void setCarHeight(int carHeight) {
        this.carHeight = carHeight;
    }

    @Override
    public void run() {
        synchronized (this) {

            try {
                while (isWaiting) {
                    try {
                        System.out.println("entrou no wait");
                        this.wait();
                    } catch (InterruptedException e) {

                    }
                }
                if (!isWaiting) {
                    this.notifyAll();
                }
                
                System.out.println("fora do wait");
                System.out.println(isWaiting);
                // Generate a random coordinate to calculate the line equation
                next_coordinate = generateRandomPoint(currentCoordiante.getX(), currentCoordiante.getY());

                // Calculate the distance to run
                distanceTravel = calculateDistanceTravelledByCoordinates(this.getCurrentCoordiante(), next_coordinate);

                // Calculate the initial speed
                speed = calculateSpeed(distanceTravel);
                //travel
                double xDifference = 0.0;
                double yDifference = 0.0;

                double newX = 0.0;
                double newY = 0.0;

                while (!isWaiting) {

                    //get the possible collisions with the cars
                    collisions = getCarCollisions();
                    if (collisions.size() > 0) {
                        for (Collision coll : collisions) {
                            Car carToCollide = coll.getCarToCollide();
                            if (carToCollide.getPriority() <= this.getPriority()) {
                                if (coll.getTimeOfCollision() < 5) {
                                    map.getCars().get(carToCollide).setSpeed(0);

                                } else if (coll.getTimeOfCollision() < 8) {
                                    map.getCars().get(carToCollide).setSpeed(carToCollide.getSpeed() - 5);

                                } else {
                                    map.getCars().get(carToCollide).setSpeed(carToCollide.getSpeed() - 2);
                                }

                            } else {
                                if (coll.getTimeOfCollision() < 5) {
                                    map.getCars().get(this.idCar).setSpeed(0);

                                } else if (coll.getTimeOfCollision() < 8) {
                                    map.getCars().get(this.idCar).setSpeed(carToCollide.getSpeed() - 5);

                                } else {
                                    map.getCars().get(this.idCar).setSpeed(carToCollide.getSpeed() - 2);
                                }
                            }

                        }
                    }

                    System.out.println("ID CAR" + this.getIdCar() + " :" + currentCoordiante.getX() + "/" + currentCoordiante.getY());
                    xDifference = currentCoordiante.getX() - next_coordinate.getX();
                    yDifference = currentCoordiante.getY() - next_coordinate.getY();
                    if (xDifference == 0) {
                        if (yDifference < 0) {
                            // está a subir
                            newY = currentCoordiante.getY() + increment;
                            System.out.println("Está a subir");
                        } else if (yDifference > 0) {
                            // está a descer
                            newY = currentCoordiante.getY() - increment;
                            System.out.println("Está a descer");
                        } else {
                            // carro parado
                            System.out.println("carro parado");
                            newX = currentCoordiante.getX();
                            newY = currentCoordiante.getY();
                        }
                    } //andar para a direita
                    else if (xDifference < 0) {
                        if (yDifference < 0) {
                            // está a subir
                            System.out.println("Está a subir e a ir para a direita");
                            newX = currentCoordiante.getX() + increment;
                            newY = currentCoordiante.getY() + increment;
                        } else if (yDifference > 0) {
                            // está a descer
                            System.out.println("Está a descer e a ir para a direita");
                            newX = currentCoordiante.getX() + increment;
                            newY = currentCoordiante.getY() - increment;
                        } else {
                            // carro parado
                            System.out.println("Está parado e a ir para a direita");
                            newX = currentCoordiante.getX();
                            newY = currentCoordiante.getY();
                        }
                    } //andar para a esquerda
                    else {
                        System.out.println("else");
                        if (yDifference < 0) {
                            // está a subir
                            newX = currentCoordiante.getX() - increment;
                            newY = currentCoordiante.getY() + increment;

                            System.out.println("Está a subir e a ir para a esquerda");
                        } else if (yDifference > 0) {
                            // está a descer
                            newX = currentCoordiante.getX() - increment;
                            newY = currentCoordiante.getY() - increment;
                            System.out.println("Está a descer e a ir para a direita");
                        } else {
                            // carro parado
                            System.out.println("Está parado");
                            newX = currentCoordiante.getX();
                            newY = currentCoordiante.getY();
                        }
                    }

                    double x_rounded = round(next_coordinate.getX());
                    double y_rounded = round(next_coordinate.getY());

                    currentCoordiante.setX(x_rounded);
                    currentCoordiante.setY(y_rounded);

                    next_coordinate.setX(newX);
                    next_coordinate.setY(newY);

                    Car.sleep(1000); // this is only to 
                }

            } catch (InterruptedException e) {
                Logger.getLogger(Car.class.getName()).log(Level.SEVERE, null, e);
            }

        }

    }

    private synchronized Coordinate generateRandomPoint(double x_coordinate, double y_coordinate) {

        //Gerar limites do número random
        double start = 0.0;
        double end = 5.0;

        Random random = new Random();

        double x_random = start + (end - start) * random.nextDouble();
        double y_random = start + (end - start) * random.nextDouble();

        while (x_random == 0.0 && y_random == 0.0) {
            x_random = start + (end - start) * random.nextDouble();
            y_random = start + (end - start) * random.nextDouble();
        }

        double result_x = round(x_random);
        double result_y = round(y_random);

        double x_coordinate_result = x_coordinate + result_x;
        double y_coordinate_result = y_coordinate + result_y;

        next_coordinate = new Coordinate(x_coordinate_result, y_coordinate_result);

        System.out.println(x_coordinate_result + "/" + y_coordinate_result);

        return next_coordinate;
    }

    private synchronized double calculateDistanceTravelledByCoordinates(Coordinate current_coordinate, Coordinate next_coordinate) {
        double valX = Math.pow((next_coordinate.getX() - current_coordinate.getX()), 2.0);
        double valY = Math.pow((next_coordinate.getY() - current_coordinate.getY()), 2.0);

        distanceTravel = Math.sqrt(valX + valY);

        return distanceTravel;
    }

    private synchronized double calculateSpeed(double distanceTravelled) {
        speed = (distanceTravelled / 10) * 36;
        return speed;
    }

    //detects the colision and returns the seconds that the will take place
    private synchronized double calculateNextCollisionSeconds(Coordinate current_coordinate_first, Coordinate next_coordinate_first, Coordinate current_coordinate_second, Coordinate next_coordinate_second) {
        double w0_X = current_coordinate_first.getX() - current_coordinate_second.getX();
        double w0_Y = current_coordinate_first.getY() - current_coordinate_second.getY();

        double uX = next_coordinate_first.getX() - current_coordinate_first.getX();
        double uY = next_coordinate_first.getY() - current_coordinate_first.getY();

        double vX = next_coordinate_second.getX() - current_coordinate_second.getX();
        double vY = next_coordinate_second.getY() - current_coordinate_second.getY();

        double u_vX = uX - vX;
        double u_vY = uY - vY;

        double u_vX_squared = Math.pow(u_vX, 2.0);
        double u_vY_squared = Math.pow(u_vY, 2.0);

        double numerator = (-w0_X * u_vX) + (-w0_Y * u_vY);
        double sqrt_denominator = Math.sqrt(u_vX_squared + u_vY_squared);
        double denominator = Math.pow(sqrt_denominator, 2.0);

        return numerator / denominator;
    }

    public synchronized ArrayList<Collision> getCarCollisions() {
        ArrayList<Collision> collisionsAux = new ArrayList<>();
        Collision coll = null;

        //falta lock da memoria partilhada
        HashMap<Integer, Car> listCar = map.getCars();
        Set<Integer> keyset = listCar.keySet();

        for (Integer key : keyset) {
            if (key != this.idCar) {
                Car carToCollide = map.getCars().get(key);
                double timeToCollide = calculateNextCollisionSeconds(this.currentCoordiante, this.next_coordinate, carToCollide.getCurrentCoordiante(), carToCollide.getNext_coordinate());
                if (getCoordinateByCollisionTime(timeToCollide, this.currentCoordiante, this.next_coordinate, carToCollide.getCurrentCoordiante(), carToCollide.getNext_coordinate())) {
                    coll = new Collision(new Double(timeToCollide).longValue(), carToCollide);
                    collisionsAux.add(coll);
                }
            }
        }

        Collections.sort(collisions, new Comparator<Collision>() {

            @Override
            public int compare(Collision o1, Collision o2) {
                if (o1.getTimeOfCollision() > o2.getTimeOfCollision()) {
                    return 1;
                } else if (o1.getTimeOfCollision() == o2.getTimeOfCollision()) {
                    return 0;
                } else {
                    return -1;
                }

            }
        });

        return collisionsAux;
    }

    private synchronized boolean getCoordinateByCollisionTime(double timeToCollide, Coordinate current_coordinate_first, Coordinate next_coordinate_first, Coordinate current_coordinate_second, Coordinate next_coordinate_second) {
        // Reta do 1º carro
        double uX = next_coordinate_first.getX() - current_coordinate_first.getX();
        double uY = next_coordinate_first.getY() - current_coordinate_first.getY();

        double result_x = timeToCollide * uX + current_coordinate_first.getX();
        double result_y = timeToCollide * uY + current_coordinate_first.getY();

        Coordinate c1 = new Coordinate(result_x, result_y);

        // Reta do 2º carro
        double vX = next_coordinate_second.getX() - current_coordinate_second.getX();
        double vY = next_coordinate_second.getY() - current_coordinate_second.getY();

        result_x = timeToCollide * vX + current_coordinate_second.getX();
        result_y = timeToCollide * vY + current_coordinate_second.getY();

        Coordinate c2 = new Coordinate(result_x, result_y);

        if (c1.getX() == c2.getX() && c1.getY() == c2.getY()) {
            return true;
        }

        return false;
    }

    public static double round(double coordinate) {
        long factor = (long) Math.pow(10, 1);
        coordinate = coordinate * factor;
        long tmp = Math.round(coordinate);
        return (double) tmp / factor;
    }

}
