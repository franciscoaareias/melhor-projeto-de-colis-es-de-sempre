/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tiagopereira92
 */
public class Collision {

    private long timeOfCollision;
    private Car carToCollide;

    public Collision() {
    }
    
    public Collision(long timeOfCollision, Car carToCollide)
    {
        this.timeOfCollision = timeOfCollision;
        this.carToCollide = carToCollide;
        
    }
    public long getTimeOfCollision() {
        return timeOfCollision;
    }

    public void setTimeOfCollision(long timeOfCollision) {
        this.timeOfCollision = timeOfCollision;
    }

    public Car getCarToCollide() {
        return carToCollide;
    }

    public void setCarToCollide(Car carToCollide) {
        this.carToCollide = carToCollide;
    }

}
