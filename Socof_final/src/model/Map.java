package model;

import java.util.HashMap;

public class Map {

    /*Instancia Singleton*/
    private static Map mapa;
    //Variables of the Map
    private final int xlimit;
    private final int ylimit;
    private final int zlimit;
    //Variables of the Number of Cars
    private int incrementalNumberOfCars;
    private int NumberOfCars;
    private HashMap<Integer, Car> cars; //cars that are in the map
   
    
    public synchronized HashMap<Integer, Car> getCars() {
        return cars;
    }

    public void setCars(HashMap<Integer, Car> cars) {
        this.cars = cars;
    }

    //SINGLETON
    private Map() {
        cars = new HashMap();
        xlimit = 1000;
        ylimit = 1000;
        zlimit = 1000;
        incrementalNumberOfCars = 0;
        NumberOfCars = 0;
        
    }

    public static Map getInstance() {

        if (mapa == null) {
            mapa = new Map();
        }
        return mapa;
    }

    //Gets and Sets
    public int getIncrementalNumberOfCars() {
        return incrementalNumberOfCars;
    }

    public void setIncrementalNumberOfCars(int incrementalNumberOfCars) {
        this.incrementalNumberOfCars = incrementalNumberOfCars;
    }

    public int getNumberOfCars() {
        return NumberOfCars;
    }

    public void setNumberOfCars(int NumberOfCars) {
        this.NumberOfCars = NumberOfCars;
    }

    public int getWidth() {
        return xlimit;
    }

    public int getLength() {
        return ylimit;
    }

    public int getHeight() {
        return zlimit;
    }

    public void addMapCarro(Car car) {
        if (!cars.containsKey(car.getIdCar())) {
            cars.put(car.getIdCar(), car);
        }
    }

    public void deleteMapCar(int idCar) {
        if (cars.containsKey(idCar)) {
            cars.remove(idCar);
        }
    }

}
