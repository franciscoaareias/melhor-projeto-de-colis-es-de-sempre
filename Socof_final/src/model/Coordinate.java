package model;

public class Coordinate {
    
    //Variables
    private double x;
    private double y;
    private double z;
    
    //Constructors
    public Coordinate(){}
    
    public Coordinate(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public Coordinate(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Gets and Sets
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    
}
