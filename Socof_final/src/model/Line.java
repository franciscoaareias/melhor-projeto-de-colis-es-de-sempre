package model;

public class Line {
    // y = m * x + b
    private double slope; // m
    private double constant_Term; //b

    public Line(double slope, double constant_Term) {
        this.slope = slope;
        this.constant_Term = constant_Term;
    }

    public Line() {
    }
    
    public double getConstant_Term() {
        return constant_Term;
    }

    public void setConstant_Term(double constant_Term) {
        this.constant_Term = constant_Term;
    }

    public double getSlope() {
        return slope;
    }

    public void setSlope(double slope) {
        this.slope = slope;
    }

}
