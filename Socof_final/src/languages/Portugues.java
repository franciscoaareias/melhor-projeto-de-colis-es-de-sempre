package languages;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Portugues {

    private static final Map<String, String> myMap;

    static {
        Map<String, String> Translate = new HashMap<String, String>();

      
        /* Menu Inicial */
        Translate.put("title","O Caminho para o Inferno");
      

        /* Menu Bar */
        Translate.put("menu_opt_cars","Carros");
        Translate.put("menu_opt_options","Opçőes");

      
        /* Menu Items MENU BAR 1 */
        Translate.put("menu_cars_opt_add","Adicionar Novo Carro");
        Translate.put("menu_cars_opt_edit","Editar Novo Carro");

      
        /* Menu Items MENU BAR 2 */
        Translate.put("menu_options_opt_sounds","Sons");
        Translate.put("menu_options_opt_music","Música");
        Translate.put("menu_options_opt_lang","Linguagem");
        
        /* Tracks */
        Translate.put("track_1","Música 1");
        Translate.put("track_2","Música 2");
        Translate.put("track_3","Música 3");
      
        /* Linguas */
        Translate.put("lang_pt","Português");
        Translate.put("lang_en","English");
      

        /* Phrases of Action */
        Translate.put("action_add","Adicionar Novo Carro");
        Translate.put("action_edit","Editar Novo Carro");

      
        /* Mensagem de Erro Crítica */
        Translate.put("msg_error","Algo de errado se passou. Pedimos desculpa pelo inconveniente.");
        Translate.put("msg_success_creation","Carro criado com sucesso! \n");

      
        /* Menu JEditACar */
        Translate.put("edit_car_id","O Carro com o ID: ");
        Translate.put("edit_car_success"," foi editado com sucesso");
        Translate.put("edit_car_cars_id","ID do Carro: ");
        Translate.put("edit_car_with_id","O Carro com o ID: ");
        Translate.put("edit_car_erased"," foi apagado com sucesso");
        Translate.put("edit_car_id_2","ID do Carro: ");

      
        /* JAddNewCar - Mensagens */
        Translate.put("add_new_car_id","ID: ");
        Translate.put("add_new_car_coordinates","\n Coordenadas: ");
        Translate.put("add_new_car_choice","\n Carro: ");

      
        /* Menu JAddNewCar */
        Translate.put("menu_add_car_add","Adicionar Novo Carro");
        Translate.put("menu_add_car_id","ID do Novo Carro: ");
        Translate.put("menu_add_car_coordinates","Coordenadas do Carro: ");
        Translate.put("menu_add_car_x","Coordenada X: ");
        Translate.put("menu_add_car_y","Coordenada Y: ");
        Translate.put("menu_add_car_z","Coordenada Z: ");
        Translate.put("menu_add_car_car","Carro: ");
        Translate.put("menu_add_car_create","Criar Novo Carro");
        Translate.put("menu_add_car_cancel","Cancelar");


        /* Menu JEditACar */
        Translate.put("menu_edit_car_edit","Editar um Carro");
        Translate.put("menu_edit_car_choose","Escolher um Carro: ");
        Translate.put("menu_edit_car_coordinates","Coordenadas do Carro: ");
        Translate.put("menu_edit_car_x","Coordenada X: ");
        Translate.put("menu_edit_car_y","Coordenada Y: ");
        Translate.put("menu_edit_car_z","Coordenada Z: ");
        Translate.put("menu_edit_car_car","Carro: ");
        Translate.put("menu_edit_car_edit_bt","Editar");
        Translate.put("menu_edit_car_cancel","Cancelar");
        Translate.put("menu_edit_car_delete","Apagar");


        /* Controllers */
        Translate.put("msg_controller_x", "A Coordenada X năo é um número \n");
        Translate.put("msg_controller_y", "A Coordenada Y năo é um número \n");
        Translate.put("msg_controller_z", "A Coordenada Z năo é um número \n");
        Translate.put("msg_controller_width", "A Largura năo é um número \n");
        Translate.put("msg_controller_length", "O Comprimento năo é um número \n");
        Translate.put("msg_controller_height", "A Altura năo é um número \n");

        /* Vehicules */
        Translate.put("bus","Autocarro");
        Translate.put("car","Carro");
        Translate.put("ferrari","Ferrari");
        Translate.put("yellow_truck","Camião Amarelo");
        
        
        
        myMap = Collections.unmodifiableMap(Translate);
    }


    public static String getText(String indication) 
    {
        return myMap.get(indication);
    }

}