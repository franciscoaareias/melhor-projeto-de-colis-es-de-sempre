package Languages;

import languages.English;
import languages.Portugues;


public class Language {

    private String lang;
    private static Language instance;

    private Language() {

        // Reads the property file and searches the language, if it doesn't exists in the property file
        // it's defined
        lang = "en";

    }
    
    public static Language getInstance(){
        /* Singleton */
        if(instance == null) {
            instance = new Language();
        }
        
        return instance;
    } 
    
    
    public String getText(String indication) {

        String return_value = "";
        switch (lang) {
            case "pt":
                return_value = Portugues.getText(indication);
                break;
            default:
            case "en":
                return_value = English.getText(indication);
                break;
        }

        return return_value;

    }

    public void setLang(String lang) {
        this.lang = lang;
    }
    
    
}
