package languages;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class English {
        
    private static final Map<String, String> myMap;
    static {
        Map<String, String> Translate = new HashMap<String, String>();
        
        /* Main Menu */
        Translate.put("title","Highway to Hell");
        
        /* Menu Bar */
        Translate.put("menu_opt_cars","Cars");
        Translate.put("menu_opt_options","Options");
        
        /* Menu Items MENU BAR 1 */
        Translate.put("menu_cars_opt_add","Add a new Car");
        Translate.put("menu_cars_opt_edit","Edit Car");
        
        /* Menu Items MENU BAR 2*/
        Translate.put("menu_options_opt_sounds","Sounds");
        Translate.put("menu_options_opt_music","Music");
        Translate.put("menu_options_opt_lang","Language");
        
        /* Linguas */
        Translate.put("lang_pt","Português");
        Translate.put("lang_en","English");
        
        /* Tracks */
        Translate.put("track_1","Track 1");
        Translate.put("track_2","Track 2");
        Translate.put("track_3","Track 3");
        
        /* Phrases of Action */
        Translate.put("action_add","Adding a new Car");
        Translate.put("action_edit","Editing a Carro");
        
        /* Mensagem de Erro Crítica */
        Translate.put("msg_error","Something went wrong. We are sorry for the inconvenient.");
        Translate.put("msg_success_creation","You have successfully Created a Car \n");
        
        /* Menu JEditACar */
        Translate.put("edit_car_id","The Car with the ID: ");
        Translate.put("edit_car_success"," was edited successfully");
        Translate.put("edit_car_cars_id","Car's Id: ");
        Translate.put("edit_car_with_id","The Car with the ID: ");
        Translate.put("edit_car_erased"," was errased successfully");
        Translate.put("edit_car_id_2","Car's Id: ");
        
        /* JAddNewCar - Mensagens */
        Translate.put("add_new_car_id","ID: ");
        Translate.put("add_new_car_coordinates","\n Coordinates: ");
        Translate.put("add_new_car_choice","\n Car: ");
        
        /* Menu JAddNewCar */
        Translate.put("menu_add_car_add","Adding new Car");
        Translate.put("menu_add_car_id","New Car's ID: ");
        Translate.put("menu_add_car_coordinates","Car Coordinates: ");
        Translate.put("menu_add_car_x","Coordinate X: ");
        Translate.put("menu_add_car_y","Coordinate Y: ");
        Translate.put("menu_add_car_z","Coordinate Z: ");
        Translate.put("menu_add_car_car","Car: ");
        Translate.put("menu_add_car_create","Create new Car");
        Translate.put("menu_add_car_cancel","Cancel");
        
        /* Menu JEditACar */
        Translate.put("menu_edit_car_edit","Editing a Car");
        Translate.put("menu_edit_car_choose","Choose a Car: ");
        Translate.put("menu_edit_car_coordinates","Car Coordinates: ");
        Translate.put("menu_edit_car_x","Coordinate X: ");
        Translate.put("menu_edit_car_y","Coordinate Y: ");
        Translate.put("menu_edit_car_z","Coordinate Z: ");
        Translate.put("menu_edit_car_car","Car: ");
        Translate.put("menu_edit_car_edit_bt","Edit");
        Translate.put("menu_edit_car_cancel","Cancel");
        Translate.put("menu_edit_car_delete","Delete");
        
        /* Controllers */
        Translate.put("msg_controller_x", "The X Coordinate is not a number \n");
        Translate.put("msg_controller_y", "The Y Coordinate is not a number \n");
        Translate.put("msg_controller_z", "The Z Coordinate is not a number \n");
        Translate.put("msg_controller_width", "The Width is not a number \n");
        Translate.put("msg_controller_length", "The Length is not a number \n");
        Translate.put("msg_controller_height", "The Height is not a number \n");
        
        /* Vehicules */
        Translate.put("bus","Bus");
        Translate.put("car","Car");
        Translate.put("ferrari","Ferrari");
        Translate.put("yellow_truck","Yellow Truck");
        
        myMap = Collections.unmodifiableMap(Translate);
    }

    public static String getText(String indication) 
    {
        return myMap.get(indication);
    }
}