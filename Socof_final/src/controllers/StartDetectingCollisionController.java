package controllers;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import model.Car;
import model.Coordinate;
import model.Map;

public class StartDetectingCollisionController {

    private static StartDetectingCollisionController instance;
    private Map map; // the map structure
    private boolean isWaiting;
    private final Object lock = new Object();
    private StartDetectingCollisionController() {
        map = Map.getInstance();
        isWaiting = false;
    }

    public static StartDetectingCollisionController getInstance() {
        if (instance == null) {
            instance = new StartDetectingCollisionController();
        }
        return instance;
    }

    public void initializeCars() {
        //initialize all cars threads.
        for (Integer car : map.getCars().keySet()) {
            map.getCars().get(car).start();
        }

    }

    public Map getMap() {
        if (map == null) {
            map = Map.getInstance();
        }
        return map;
    }

    /**
     *
     * @return the current positions of the cars that are represented in the map
     */
    public ArrayList<Coordinate> getCurrentCoordinates() {
        ArrayList<Coordinate> coordinates = new ArrayList();

        for (Integer key : map.getCars().keySet()) {
            coordinates.add(map.getCars().get(key).getCurrentCoordiante());
        }

        return coordinates;
    }

    public ArrayList<Coordinate> getNextCoordinates() {
        ArrayList<Coordinate> coordinates = new ArrayList();

        for (Integer key : map.getCars().keySet()) {
            coordinates.add(map.getCars().get(key).getNext_coordinate());
        }

        return coordinates;
    }

    public synchronized void startDetectingCollisionThreads() {
        HashMap<Integer, Car> listCar = getMap().getCars();
        Set<Integer> keyset = listCar.keySet();

        Iterator<Integer> it = keyset.iterator();
        while (it.hasNext()) {
            if (!isWaiting) {
                 listCar.get(it.next()).start();
                
            } else {
                listCar.get(it.next()).setIsWaiting(false);
            }
        }
        isWaiting = false;
    }

    public synchronized void stopDetectingCollisionThreads() {
        try {
        
            HashMap<Integer, Car> listCar = getMap().getCars();
            Set<Integer> keyset = listCar.keySet();

            Iterator<Integer> it = keyset.iterator();
            while (it.hasNext()) {
               listCar.get(it.next()).setIsWaiting(true);
            }
             isWaiting = true;
        } catch (Exception e) {
            System.out.println("e" + e);
        }
    }
}
