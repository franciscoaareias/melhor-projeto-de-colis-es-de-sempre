package controllers;

import Languages.Language;
import java.util.HashMap;
import java.util.Set;
import model.Car;
import model.Coordinate;
import model.Map;

public class MainController {

    private final Map map;
    //Mensagem Erro
    private String error_message = "";
    private boolean status = true;
    private final Language lang;
    private static MainController instance;

    private MainController() {
        map = Map.getInstance();
        lang = Language.getInstance();
    }

    public static MainController getInstance() {
        if (instance == null) {
           
            instance = new MainController();

        }
        return instance;
    }

    //Método para verificar se a String é double
    public boolean verifyDouble(String x) {
        double number;
        try {
            number = Double.parseDouble(x);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //Método que retorna a mensagem de erro
    public String getErrorMessage() {
        return error_message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getNumberOfCars() {
        return map.getIncrementalNumberOfCars();
    }

    public void create(int idCar, String coordenadaX, String coordenadaY, String coordenadaZ, int carWidth, int carLength, int carHeight, int imageSelected) {
        //coordenadas
        double coordinate_x_axis = Double.parseDouble(coordenadaX);
        double coordinate_y_axis = Double.parseDouble(coordenadaY);
        double coordinate_z_axis = Double.parseDouble(coordenadaZ);
        //Criar Coordenada
        Coordinate c = new Coordinate(coordinate_x_axis, coordinate_y_axis, coordinate_z_axis);

        
        /* CRIAR NOVO CARRO */
        Car new_car = new Car(idCar, c, carWidth, carLength, carHeight,Double.valueOf(idCar).longValue());
        new_car.setCarImage(imageSelected);

        //Add new Car to the incremental Number
        map.setIncrementalNumberOfCars(map.getIncrementalNumberOfCars() + 1);
        map.setNumberOfCars(map.getNumberOfCars() + 1);

        //Add the new car to the map
        map.addMapCarro(new_car);
    }

    public void edit(Car car_to_edit, int idCar, String coordenadaX, String coordenadaY, String coordenadaZ, int carWidth, int carLength, int carHeight, int imgCar) {
        //coordenadas
        double coordinate_x_axis = Double.parseDouble(coordenadaX);
        double coordinate_y_axis = Double.parseDouble(coordenadaY);
        double coordinate_z_axis = Double.parseDouble(coordenadaZ);

        //Criar Coordenada
        Coordinate c = new Coordinate(coordinate_x_axis, coordinate_y_axis, coordinate_z_axis);

//      /* Editar o carro */
        car_to_edit.setCurrentCoordiante(c);
        car_to_edit.setCarWidth(carWidth);//passa a ser a image.
        car_to_edit.setCarLength(carLength);
        car_to_edit.setCarHeight(carHeight);
        car_to_edit.setCarImage(imgCar);
        
        map.getCars().put(car_to_edit.getIdCar(), car_to_edit);
        

    }

    public void deleteCar(int idCar) {
        try {
            //Eliminar o carro do mapa
            map.deleteMapCar(idCar);

            //Reduzir o número de carros no mapa
            map.setNumberOfCars(map.getNumberOfCars() - 1);

            status = true;
        } catch (Exception e) {
            status = false;

            error_message = lang.getText("mensagem_critica");
        }
    }
    
    public HashMap<Integer,Car> getCars()
    {
        return map.getCars();
       
    }
    
    public Set<Integer> getIdOfCars()
    {
        return map.getCars().keySet();
        
    }
    
    public boolean containsIdCar(int idCar)
    {
        return map.getCars().containsKey(idCar);
    }
    
    public Car getCar(int idCar)
    {
        if(map.getCars().containsKey(idCar))
        {
            return map.getCars().get(idCar);
            
        }
        return null;
    }
   
}
